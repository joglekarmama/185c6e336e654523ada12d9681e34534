import React, { useEffect } from 'react';
import config from 'visual-config-exposer';
import Styled from 'styled-components';
import { Howl, Howler } from 'howler';

import './SecondScreen.css';

const Sound = config.settings.sound;

const sound = new Howl({
  src: [Sound],
});

const Second = Styled.div`
background: url(${config.settings.secondImage});
`;

const Button = Styled.button`
background-color: ${config.settings.btnBackground};
color: ${config.settings.btnFontColor};
`;

const Heading = Styled.h1`
color: ${config.settings.secondTextColor};
`;

const SecondScreen = (props) => {
  useEffect(() => {
    if (props.soundOn && config.settings.soundPlayed) {
      sound.play();
    }
  }, []);

  return (
    <Second className="second-screen">
      <Heading className="second-heading">{config.settings.secondText}</Heading>
      <div>
        <Button onClick={props.back} className="second-btn">
          Back
        </Button>
      </div>
    </Second>
  );
};

export default SecondScreen;
